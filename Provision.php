<?php

/**
 * Class Provision
 */
class Provision {

    /**
     * @var string
     */
    private $gatewayIp;
    /**
     * @var bool
     */
    private $secure;

    ///////////////////////////////////////////
    //
    // Publicly accessible methods
    //
    ///////////////////////////////////////////

    /**
     * Provisioner constructor.
     *
     * @param string $gatewayIp
     * @param bool $secure
     */
    public function __construct($gatewayIp = '192.168.4.14', $secure = false) {
        /*
         * TODO: What if an invalid format IP address or domain name, and non-string type is provided
         * TODO: as the 1st parameter and a non-boolean type provided for the 2nd parameter?
         */

        $this->gatewayIp = $gatewayIp;
        $this->secure = $secure;
    }

    /**
     * Creates JSON suitable for the agent described by the method params.
     * Returns the JSON response from the Web Gateway.
     *
     * @param $username
     * @param $domain
     * @param $topic
     * @return mixed
     */
    public function provisionAgent($username, $domain, $topic) {
        $json = $this->buildAgentJson($username, $domain, $topic);

        return $this->provision($json);
    }

    ///////////////////////////////////////////
    //
    // Internal method to fulfill the above API
    //
    ///////////////////////////////////////////

    /**
     * Builds provisioning JSON suitable for an agent.
     *
     * @param $username
     * @param $domain
     * @param $topic
     * @return string
     */
    private function buildAgentJson($username, $domain, $topic) {
        // TODO: What are other, more flexible and safer ways to construct a JSON object? What about the WebAppId being dynamic?
        $json = '{
            "webAppId": "454c093f-7f18-4d24-b731-7205343255ad",
            "allowedOrigins": ["*"],
            "urlSchemeDetails": {
                "secure": %s,
                "host": "%s",
                "port": "%s"
            },
            "voice": {
                "username": "%s",
                "domain": "%s"
            },
            "additionalAttributes": {
                "AED2.allowedTopic": "%s",
                "AED2.metadata": {
                    "features": ["zoom","annotate","spotlight","document-share"],
                    "role": "agent",
                    "name": "Blah",
                    "permission": {
                        "viewable": ["default", "interact"],
                        "interactive": ["default"]
                    }
                }
            }
        }';


        $secureString = $this->secure ? 'true' : 'false';
        $portString = $this->secure ? '8443' : '8080';

        return sprintf($json, $secureString, $this->gatewayIp, $portString, $username, $domain, $topic);
    }

    /**
     * Sends the given provisioning JSON to the Web Gateway.
     * Returns the JSON received from the Web Gateway.
     *
     * @param $json
     * @return mixed
     */
    private function provision($json) {
        // TODO: How can you check if this is a valid JSON object in the parameter?

        // Configure the curl options
        $ch = curl_init($this->buildGatewayUri());

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json)
        ]);

        // Execute HTTP POST & close the connection
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
     * Returns a URI corresponding to the address of the Web Gateway.
     *
     * @return string
     */
    private function buildGatewayUri () {
        return $this->secure
            ? 'https://' . $this->gatewayIp . ':8443/gateway/sessions/session'
            : 'http://' . $this->gatewayIp . ':8080/gateway/sessions/session';
    }
}